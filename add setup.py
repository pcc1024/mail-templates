from setuptools import setup

setup(
    name="send_mail_template",
    version="0.0.1",
    description="Send mail with python with sendgrid and mailjet",
    keywords="mail sendgrid mailjet",
    url="https://gitlab.com/pcc1024/mail-templates.git",
    author="pierre.c.cardona@gmail.com",
    packages=["send_mail_tpl"],
    zip_safe=True,
    include_package_data=True,
    #install_requires=["mailjet_rest==1.3.0", "Jinja2==2.10.1"], Fill with requirements
)
